package chess;

/**
 * Queen class
 *
 * @author Ashleigh Chung
 */
public class Queen extends Piece {

    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public static Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }

    /**
     * Queen Constructor
     *
     * @param w - whether it is white or not
     * @param n - name
     */
    public Queen(boolean w, String n) {
        super(w, n);
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded Queen Constructor for copying purposes
     *
     * @param other - the queen to be copied from
     */
    public Queen(Queen other){
        super(other);
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this queen can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty){

        /**
         * Difference in col between dest. and origin
         */
        int absX = Math.abs(oldC - newC);

        /**
         * Difference in row between dest. and origin
         */
        int absY = Math.abs(oldR - newR);

        if(absX == absY)
            return true;
        else if((absX != 0 && absY == 0) || (absX == 0 && absY != 0))
            return true;
        else
            return false;

    }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC){
        this.refreshSquaresTouched();

        /**
         * Whether or not Queen goes straight or diagonally
         */
        boolean isStraight;

        /**
         * Difference in col in dest. and origin
         */
        int absX = Math.abs(oldC - newC);

        /**
         * Difference in row in dest. and origin
         */
        int absY = Math.abs(oldR - newR);

        /**
         * Number of squares between dest. and origin
         */
        int size;

        if(absX == 0 || absY == 0) isStraight = true;
        else isStraight = false;

        // If Queen moves in a straight line
        if(isStraight == true){

            /**
             * Whether or not row or col changes when Queen goes straight, and if row # should increment or decrement
             */
            boolean rowChanges, incrementR = true, incrementC = true;

            // 1. Find out whether the row or column changes
            if(absX == 0) rowChanges = true;
            else rowChanges = false;

            // 2. Determine size of array
            //    If oldR -> newR goes up/down OR oldC -> newC goes left/right
            if(rowChanges){
                size = Math.abs(oldR - newR) - 1;
                if(oldR > newR) incrementR = false;
                else incrementR = true;
            }
            else {
                size = Math.abs(oldC - newC) - 1;
                if(oldC > newC) incrementC = false;
                else incrementC = true;
            }

            // 3. Add the points to the array while appropriately incrementing/decrementing
            for(int i = 0; i < size; i++){
                if(rowChanges){
                    if(incrementR) oldR++;
                    else oldR--;
                }
                else{
                    if(incrementC) oldC++;
                    else oldC--;
                }
                squaresTouched[i] = new Point(oldR, oldC);
            }
        }

        // If Queen moves diagonally
        else{
            /**
             * Whether to increment/decrement row and col
             */
            boolean incrementR, incrementC;
            size = Math.abs(oldR - newR) - 1;

            // 1. Whether the direction increments or decrements in terms of row and col
            if(oldR > newR) incrementR = false;
            else incrementR = true;

            if(oldC > newC) incrementC = false;
            else incrementC = true;

            // 2. Add the points to the array while appropriately incrementing/decrementing
            for(int i = 0; i < size; i++){
                if(incrementR) oldR++;
                else oldR--;

                if(incrementC) oldC++;
                else oldC--;

                this.squaresTouched[i] = new Point(oldR, oldC);
            }
        }
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col){
        this.validMovesList = new Point[64];

        /**
         * Index for validMovesList
         */
        int counter = 0;

        //diagonal

        /**
         * row and col going towards top left
         */
        int r1 = row - 1, c1 = col - 1;
        while(r1 >= 0 && c1 >= 0){
            this.validMovesList[counter] = new Point(r1, c1);
            counter++;
            r1--;
            c1--;
        }

        /**
         * row and col going towards bottom right
         */
        int r2 = row + 1, c2 = col + 1;
        while(r2 <= 7 && c2 <= 7){
            this.validMovesList[counter] = new Point(r2, c2);
            counter++;
            r2++;
            c2++;
        }

        /**
         * row and col going towards top right
         */
        int r3 = row - 1, c3 = col + 1;
        while(r3 >= 0 && c3 <= 7){
            this.validMovesList[counter] = new Point(r3, c3);
            counter++;
            r3--;
            c3++;
        }

        /**
         * row and col going towards bottom left
         */
        int r4 = row + 1, c4 = col - 1;
        while(r4 <= 7 && c4 >= 0){
            this.validMovesList[counter] = new Point(r4, c4);
            counter++;
            r4++;
            c4--;
        }

        //straight
        for(int i = 0; i < 8; i ++){
            if(i != row){
                this.validMovesList[counter] = new Point(i, col);
                counter++;
            }
        }
        for(int i = 0; i < 8; i ++){
            if(i != col){
                this.validMovesList[counter] = new Point(row, i);
                counter++;
            }
        }
    }
}