package chess;
import java.io.*;

/**
 * This Chess program implements an application that runs a fully
 * functional chess game between two players.
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 *
 */
public class Chess {

    /**
     * Used to read in input
     */
    public static BufferedReader br;

    /**
     * Instance of a Board Object
     */
    public static Board gameBoard;

    /**
     * As long as the input did not call for the game to end, the player turn service continues
     */
    public static boolean endGame = false;

    /**
     * Tells the main method whether or not to print the Board Object
     */
    public static boolean printBoard = true;

    /**
     * If it is Black player's or White player's turn
     */
    public static boolean isWhitesTurn = true;

    /**
     * What the player inputs when making a move
     */
    public static String moveInput = null;

    /**
     * If the move input is not illegal - tells main whether to print illegal error
     */
    public static boolean noIllegal = true;

    /**
     * Turn counter (used for en passant, where timing is important)
     */
    public static int turn = 0;


    /**
     * The main class is what
     * initializes a board and runs the game.
     *
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException {
        //create an instance of a game
        br = new BufferedReader(new InputStreamReader(System.in));
        gameBoard = new Board();

        /* Testing Purposes Remove Later */
    


        //loop to continue turns for a game
        while (endGame == false) {
            // 1. Print board
            if (printBoard == true) {
                gameBoard.printBoard();
                System.out.println();
            }

            // 2. Based on whose turn it is, game prompts the player for move input
            moveInput = null;
            if (isWhitesTurn == true) System.out.print("White's move: ");
            else System.out.print("Black's move: ");

            // 3. Read input and parse it for the old/new X and Y coordinates
            try {
                moveInput = br.readLine();
            } catch (IOException e) {
            }
            //System.out.println(moveInput);
            System.out.println();

            // 4. Call movePiece() method to check and execute move
            noIllegal = gameBoard.movePiece(moveInput, isWhitesTurn);

            // 5. If move is legal, move onto next turn
            // Otherwise, probe same player for another move
            if (noIllegal == true) {
                isWhitesTurn = !(isWhitesTurn);
                printBoard = true;
                turn += 1;
            } else {
                System.out.println("Illegal move, try again");
                System.out.println();
                printBoard = false;
            }
        }
    }
}


