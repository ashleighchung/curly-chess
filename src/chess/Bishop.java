package chess;

/**
 * Bishop class
 *
 * @author Ashleigh Chung
 */
public class Bishop extends Piece {

    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }

    /**
     * @param w - whether it is white or not
     * @param n - name
     */
    public Bishop(boolean w, String n) {
        super(w, n);
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded Bishop Constructor for copying purposes
     * @param other - the other bishop to be copied from
     */
    public Bishop(Bishop other){
        super(other);
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this bishop can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty){

        /**
         * Difference in col between dest. and origin
         */
        int absX = Math.abs(oldC - newC);

        /**
         * Difference in row between dest. and origin
         */
        int absY = Math.abs(oldR - newR);

        if(absX == absY)
            return true;
        else
            return false;
    }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC){
        this.refreshSquaresTouched();

        /**
         * Whether or not to increment/decrement row and col
         */
        boolean incrementR, incrementC;

        /**
         * Number of squares between dest. and col
         */
        int size = Math.abs(oldR - newR) - 1;

        // 1. Whether the direction increments or decrements in terms of row and col
        if(oldR > newR) incrementR = false;
        else incrementR = true;

        if(oldC > newC) incrementC = false;
        else incrementC = true;

        // 2. Add the points to the array while appropriately incrementing/decrementing
        for(int i = 0; i < size; i++){
            if(incrementR) oldR++;
            else oldR--;

            if(incrementC) oldC++;
            else oldC--;

            squaresTouched[i] = new Point(oldR, oldC);
        }
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     * [0][1]              [0][5]
     *          [2][3]
     * [5][0]              [6][7]
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col){

        /**
         * Index for validMovesList
         */
        int counter = 0;
        this.validMovesList = new Point[64];

        /**
         * row and col going towards top left
         */
        int r1 = row - 1, c1 = col - 1;
        while(r1 >= 0 && c1 >= 0){
            this.validMovesList[counter] = new Point(r1, c1);
            counter++;
            r1--;
            c1--;
        }

        /**
         * row and col going towards bottom right
         */
        int r2 = row + 1, c2 = col + 1;
        while(r2 <= 7 && c2 <= 7){
            this.validMovesList[counter] = new Point(r2, c2);
            counter++;
            r2++;
            c2++;
        }

        /**
         * row and col going towards top right
         */
        int r3 = row - 1, c3 = col + 1;
        while(r3 >= 0 && c3 <= 7){
            this.validMovesList[counter] = new Point(r3, c3);
            counter++;
            r3--;
            c3++;
        }

        /**
         * row and col going towards bottom left
         */
        int r4 = row + 1, c4 = col - 1;
        while(r4 <= 7 && c4 >= 0){
            this.validMovesList[counter] = new Point(r4, c4);
            counter++;
            r4++;
            c4--;
        }
    }

}