package chess;

/**
 * Point Class
 * This class represents a Point (or coordinate on the chessboard) as an object
 * Used for storing the "Points" a Piece crosses when attempting to make a move
 *
 * @author Ashleigh Chung
 */
public class Point {

    /**
     * What row of the chess board
     */
    private int row;

    /**
     * What column of the chess board
     */
    private int col;

    /**
     * Constructor that initializes the x (col) and y (row) fields
     *
     * @param row
     * @param col
     */
    public Point(int row, int col){
        this.row = row;
        this.col = col;
    }

    /**
     *
     * @return the row attribute
     */
    public int getRow(){
        return this.row;
    }

    /**
     *
     * @return the col attribute
     */
    public int getCol(){
        return this.col;
    }

    /**
     *
     * @return the Point in [row][col] format
     */
    public String toString(){
        return "[" + this.row + "][" + this.col + "]";
    }

}
