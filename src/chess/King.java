package chess;

/**
 * King class
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class King extends Piece {


    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }



    /**
     * King
     *
     * @param w - whether it is white or not
     * @param n - name
     */
    public King(boolean w, String n) {
        super(w, n);
        this.isFirstMove = true;
        this.inCheck = false;
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded King Constructor for copying purposes
     *
     * @param other the King to be copied from
     */
    public King(King other){
        super(other);
        this.isFirstMove = other.isFirstMove;
        this.inCheck = other.inCheck;
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this king can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty){
        /**
         * Whether or no this move is valid for the king
         */
        boolean result = false;

        /**
         * Difference in row between dest. and row
         */
        int y  = Math.abs(newR - oldR);

        /**
         * Difference in col between dest. and row
         */
        int x = Math.abs(newC - oldC);
        if(y <= 1 && x <= 1 && !(y == 0 && x == 0)){
            result = true;
        }
        if(result){
            this.isFirstMove = false;
        }
        return result;
    }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC){
        //purposely left empty
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     * [1][1]
     *
     * [0][0], [0][1], [0][2]
     * [1][0],       , [1][2]
     * [2][0], [2][1], [2][2]
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col){
        this.validMovesList = new Point[64];

        /**
         * Index for validMovesList
         */
        int counter = 0;

        //top row
        if(row != 0){
            if(col != 0){
                this.validMovesList[counter] = new Point(row - 1, col - 1);
                counter++;
            }
            this.validMovesList[counter] = new Point(row - 1, col);
            counter++;
            if(col != 7){
                this.validMovesList[counter] = new Point(row - 1, col + 1);
                counter++;
            }
        }

        //middle row
        if(col != 0){
            this.validMovesList[counter] = new Point(row, col - 1);
            counter++;
        }
        if(col != 7){
            this.validMovesList[counter] = new Point(row, col + 1);
            counter++;
        }

        //bottom row
        if(row != 7){
            if(col != 0){
                this.validMovesList[counter] = new Point(row + 1, col - 1);
                counter++;
            }
            this.validMovesList[counter] = new Point(row + 1, col);
            counter++;
            if(col != 7){
                this.validMovesList[counter] = new Point(row + 1, col + 1);
            }
        }
    }
}