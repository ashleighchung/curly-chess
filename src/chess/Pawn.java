package chess;

import static chess.Board.board;
import static chess.Chess.turn;

/**
 * Pawn class
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Pawn extends Piece {

    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }

    /**
     * Pawn Constructor
     *
     * @param w - whether it is white or not
     * @param n - name
     */
    public Pawn(boolean w, String n) {
        super(w, n);
        this.isFirstMove = true;
        this.isEnPassant = false;
        this.counter = 0;
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded Pawn Constructor for copying purposes
     *
     * @param other - the pawn to be copied from
     */
    public Pawn(Pawn other){
        super(other);
        this.isFirstMove = other.isFirstMove;
        this.isEnPassant = other.isEnPassant;
        this.counter = other.counter;
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this pawn can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty) {
        //Does not keep track of en passant

        /**
         * Whether or not its a valid move or not for pawn
         */
        boolean result = false;
        if (this.isWhite) {//white pawn
            if (oldC == newC && isNewSpotEmpty) {//moving straight
                if (this.isFirstMove && (newR - oldR) == -2 && board[newR+1][newC] == null) {
                    this.isEnPassant = true;
                    result = true;
                } else if ((newR - oldR) == -1) {
                    result = true;
                }
            } else if (Math.abs(newC - oldC) == 1 && ((newR - oldR) == -1)) {//go diagonal to capture
                if (isNewSpotEmpty) {//En Passant
                    result = this.enPassant(oldR, oldC, newR, newC);
                } else {
                    result = true;
                }
            }
        } else{//black pawn
            if (oldC == newC && isNewSpotEmpty) {//moving straight
                if (this.isFirstMove && (newR - oldR) == 2 && board[newR-1][newC] == null) {
                    this.isEnPassant = true;
                    result = true;
                } else if ((newR - oldR) == 1) {
                    result = true;
                }
            } else if (Math.abs(newC - oldC) == 1 && ((newR - oldR) == 1)){//go diagonal to capture
                if(isNewSpotEmpty){//En Passant
                    result = this.enPassant(oldR, oldC, newR, newC);
                }else {
                    result = true;
                }
            }
        }
        if(result){
            this.isFirstMove = false;
            this.counter = turn;
        }
        return result;
    }

    /**
     * Method to ensure the legality of an en passant type capture
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @return If this a valid display of an en passant capture
     */
    public boolean enPassant(int oldR,int oldC,int newR,int newC) {
        // 5.1 Check the validity of en passant
            if (this.isWhite) {//a white pawn for that matter
                if (Math.abs(newC - oldC) == 1 && newR == 2 && oldR == 3) {//we are moving exactly one space diagonally forward into an empty spot
                    /**
                     * Opponent's Pawn
                     */
                    Piece other_pawn = board[oldR][newC];//get this piece "below" my pawn
                    if(other_pawn == null){
                        return false;
                    }
                    if (other_pawn.name.equals("bp")) {//make sure that this piece is a black pawn
                        if (other_pawn.isEnPassant && other_pawn.counter == this.counter + 1) {//make sure that this other pawn moved 2 squares last turned
                            board[oldR][newC] = null;
                            return true;
                        }
                    }
                }
            } else {//black pawn
                if (Math.abs(newC - oldC) == 1 && newR == 5 && oldR == 4) {//we are moving exactly one space diagonally forward into an empty spot
                    /**
                     * Opponent's Pawn
                     */
                    Piece other_pawn = board[oldR][newC];//get this piece "below" my pawn
                    if(other_pawn == null){
                        return false;
                    }
                    if (other_pawn.name.equals("wp")) {//make sure that this piece is a black pawn
                        if (other_pawn.isEnPassant && other_pawn.counter == this.counter + 1) {//make sure that this other pawn moved 2 squares last turned
                            board[oldR][newC] = null;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC) {
        this.refreshSquaresTouched();

/**
 * Whether or not the pawn is moving up or moving down the board
 */
        boolean goingUp;

        if (oldR > newR) goingUp = true;
        else goingUp = false;

        //for the first pawn two squares move
        if (Math.abs(oldR - newR) > 1) {
            if (goingUp) {
                squaresTouched[0] = new Point(oldR - 1, oldC);
            }else{
                squaresTouched[0] = new Point(oldR + 1, oldC);
            }
        }
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     * [1][1]
     *
     *       , [1][1],
     * [2][0], [2][1], [2][2]
     *       , [3][1],
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col) {
        /**
         * Index of array
         */
        int counter = 0;
        this.validMovesList = new Point[64];
        for (int i = -2; i <= 2; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0) {
                    continue;
                } else if ((i == -2 || i == 2) && (j == -1 || j == 1)) {
                    continue;
                } else {
                    /**
                     * Temp row for pawn
                     */
                    int tempR = row + i;
                    /**
                     * Temp col for pawn
                     */
                    int tempC = col + j;
                    if ((tempR >= 0 && tempR <= 7) && (tempC >= 0 && tempC <= 7)) {
                        /**
                         * If the other square is empty during en passant
                         */
                        boolean other_square_isEmpty = board[tempR][tempC] == null ? true : false;
                        /**
                         * Opponent's pawn piece
                         */
                        Piece other_piece = board[tempR][tempC];
                        if (this.isWhite && i < 0) {
                            if (i == -2 && this.isFirstMove && other_square_isEmpty) {// moves 2 spot
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            }
                            else if (i == -1 && j == 0 && other_square_isEmpty) {
                                this.validMovesList[counter] = new Point(tempR, tempC);// moves directly infront
                                counter++;
                            }
                            else if (other_square_isEmpty && enPassant(row, col, tempR, tempC)) {//en passant
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            }
                            else if (!other_square_isEmpty && this.isWhite != other_piece.isWhite && other_piece.name.charAt(1) == 'K'){// capturing
                                // diagonally
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            }
                        } else if (!this.isWhite && i > 0) {
                            if (i == 2 && this.isFirstMove && other_square_isEmpty) {// moves 2 spot
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            }
                            else if (i == 1 && j == 0 && other_square_isEmpty) {
                                this.validMovesList[counter] = new Point(tempR, tempC);// moves directly infront
                                counter++;
                            }
                            else if (other_square_isEmpty && enPassant(row, col, tempR, tempC)) {//en passant
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            } else if (!other_square_isEmpty && this.isWhite != other_piece.isWhite && other_piece.name.charAt(1) == 'K') {// capturing
                                // diagonally
                                this.validMovesList[counter] = new Point(tempR, tempC);
                                counter++;
                            }
                        }
                    }
                }
            }
        }
    }
}