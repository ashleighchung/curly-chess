package chess;

/**
 * Knight class
 *
 * @author Ashleigh Chung
 */
public class Knight extends Piece {

    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }

    /**
     * Knight Constructor
     *
     * @param w - whether it is white or not
     * @param n - name
     */
    public Knight(boolean w, String n) {
        super(w, n);
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded Knight Constructor for copying purposes
     *
     * @param other - the knight to be copied from
     */
    public Knight(Knight other){
        super(other);
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this knight can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty){

        /**
         * Difference in col between dest. and origin
         */
        int absX = Math.abs(oldC - newC);

        /**
         * Difference in row between dest. and origin
         */
        int absY = Math.abs(oldR - newR);

        if ((absX == 2 && absY == 1) || (absX == 1 && absY == 2))
            return true;
        else
            return false;
    }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC){
        // purposely empty because a Knight can hop over any piece in its way
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     * [2][2]
     *
     *       , [0][1],       , [0][3],
     * [1][0],       ,       ,       , [1][4]
     *       ,       ,   x   ,       ,
     * [3][0],       ,       ,       , [3][4]
     *       , [4][1],       , [4][3],
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col){
        this.validMovesList = new Point[64];
        this.validMovesList[0] = new Point(row - 1, col - 2);
        this.validMovesList[1] = new Point(row - 2, col - 1);

        this.validMovesList[2] = new Point(row - 2, col + 1);
        this.validMovesList[3] = new Point(row - 1, col + 2);

        this.validMovesList[4] = new Point(row + 1, col - 2);
        this.validMovesList[5] = new Point(row + 2, col - 1);

        this.validMovesList[6] = new Point(row + 2, col + 1);
        this.validMovesList[7] = new Point(row + 1, col + 2);

        for(int i = 0; i < this.validMovesList.length; i++){
            if(this.validMovesList[i] != null){
                if((this.validMovesList[i].getRow() < 0 || this.validMovesList[i].getRow() > 7) || (this.validMovesList[i].getCol() < 0 || this.validMovesList[i].getCol() > 7))
                    this.validMovesList[i] = null;
            }
        }

    }
}