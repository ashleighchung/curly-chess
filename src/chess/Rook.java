package chess;

import static chess.Board.board;
import static chess.Board.lineOfSight;

/**
 * Rook class
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Rook extends Piece {



    /**
     * List of moves a Piece can validly move to based on its current position
     */
    public Point[] validMovesList;

    /**
     *
     * @return the validMovesList array
     */
    public Point[] getValidMovesList(){
        return this.validMovesList;
    }



    /**
     * Rook constructor
     *
     * @param w - whether it is white or not
     * @param n - name
     */
    public Rook(boolean w, String n) {
        super(w, n);
        this.isFirstMove = true;
        this.validMovesList = new Point[64];
    }

    /**
     * Overloaded Rook Constructor for copying purposes
     *
     * @param other - the rook to be copied from
     */
    public Rook(Rook other){
        super(other);
        this.isFirstMove = other.isFirstMove;
        this.validMovesList = new Point[64];
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this rook can validly move in that spot
     */
    public boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty){
        /**
         * Whether or not the move is valid for a rook
         */
        boolean result = false;
        if(Math.abs(newR - oldR) > 0 && Math.abs(newC - oldC) == 0){//Moving within the same row

            result = true;
        }else if(Math.abs(newR - oldR) == 0 && Math.abs(newC - oldC) > 0){//Moving within the same column
            result = true;
        }
        if(result){
            Rook focusedPiece = (Rook) board[oldR][oldC];
            focusedPiece.setSquaresTouched(oldR, oldC, newR, newC);
            Point[] squaresTouchedTest = focusedPiece.getSquaresTouched();
            Boolean line_of_sight = lineOfSight(squaresTouchedTest);
            if(line_of_sight){
                this.isFirstMove = false;
            }
        }
        return result;
    }

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public void setSquaresTouched(int oldR, int oldC, int newR, int newC){
        this.refreshSquaresTouched();

        /**
         * Difference in col between dest. and origin
         */
        int absX = Math.abs(oldC - newC);

        /**
         * Number of squares between dest. and origin
         */
        int size;

        /**
         * Whether row or col changes, and if row and col should increment/decrement
         */
        boolean rowChanges, incrementR = true, incrementC = true;

        // 1. Find out whether the row or column changes
        if(absX == 0) rowChanges = true;
        else rowChanges = false;

        // 2. Determine size of array
        //    If oldR -> newR goes up/down OR oldC -> newC goes left/right
        if(rowChanges){
            size = Math.abs(oldR - newR) - 1;
            if(oldR > newR) incrementR = false;
            else incrementR = true;
        }
        else {
            size = Math.abs(oldC - newC) - 1;
            if(oldC > newC) incrementC = false;
            else incrementC = true;
        }

        // 3. Add the points to the array while appropriately incrementing/decrementing
        for(int i = 0; i < size; i++){
            if(rowChanges){
                if(incrementR) oldR++;
                else oldR--;
            }
            else{
                if(incrementC) oldC++;
                else oldC--;
            }
            squaresTouched[i] = new Point(oldR, oldC);
        }
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     *              [0][3]
     *   [3][0]     [3][3]      [3][7]
     *              [7][3]
     *
     * @param row
     * @param col
     */
    public void setValidMovesList(int row, int col){
        /**
         * Index for validMovesList
         */
        int counter = 0;
        this.validMovesList = new Point[64];
        for(int i = 0; i < 8; i ++){
            if(i != row){
                this.validMovesList[counter] = new Point(i, col);
                counter++;
            }
        }
        for(int i = 0; i < 8; i ++){
            if(i != col){
                this.validMovesList[counter] = new Point(row, i);
                counter++;
            }
        }
    }
}