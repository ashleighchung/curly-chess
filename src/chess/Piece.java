package chess;

/**
 * Abstract class Piece that contains abstract and non abstract methods that can pertain to all pieces in the chess game
 *
 * @author Ashleigh Chung
 */
public abstract class Piece {

    /**
     * Whether it is a white or black chess piece
     */
    public boolean isWhite;

    /**
     * Initial of the chess piece (color + type)
     */
    public String name;

    /**
     * Whether a piece made its first move or not (used for castling)
     */
    public boolean isFirstMove;

    /**
     * Checks if the pawn had move 2 squares in its first move
     */
    boolean isEnPassant;

    /**
     * Keeps track what turn the pawn made its last move
     */
    int counter;

    /**
     * Keeps track if the king is in check
     */
    boolean inCheck;

    /**
     * Array full of squares a chess piece crosses when making a move
     */
    public Point[] squaresTouched;

//    /**
//     * List of moves a Piece can validly move to based on its current position
//     */
//    public static Point[] validMovesList;

    /**
     * 1) sets the color attribute for the piece object
     * 2) Adds 'w' or 'b' in front of the piece's name depending on its color
     * Ex) Piece wB = new Bishop(true, "B");
     * board[2][0] = wB;
     *
     * @param isWhite - whether it is white or not
     * @param name - name
     */
    public Piece(boolean isWhite, String name){
        this.setColor(isWhite);

        if(isWhite == true)
            this.name = "w" + name;
        else
            this.name = "b" + name;


        this.squaresTouched = new Point[16]; //only need 8
    }

    /**
     * Overloaded Piece Constructor for copying purposes
     *
     * @param other - the other piece to be copied from
     */
    public Piece(Piece other){
        this.isWhite = other.isWhite;
        this.name = other.name;
        this.squaresTouched = other.squaresTouched;
    }

    /**
     * Sets the color of the Piece
     * white == true, black == false
     *
     * @param isWhite
     */
    public void setColor(boolean isWhite){
        this.isWhite = isWhite;
    }

    /**
     *
     * @return whether it is a white piece or not
     */
    public boolean isWhite(){
        return this.isWhite;
    }

    /**
     *
     * @return its name, ex) wB
     */
    public String getPieceName() {
        return this.name;
    }

    /**
     * R = row = y value
     * C = column = x value
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     * @param isNewSpotEmpty
     * @return whether or not this piece can validly move in that spot
     */
    public abstract boolean checkMove(int oldR, int oldC, int newR, int newC, boolean isNewSpotEmpty);

    /**
     * Fills in the squares a Piece would touch if it were to move to a different spot in an array
     *
     * @param oldR
     * @param oldC
     * @param newR
     * @param newC
     */
    public abstract void setSquaresTouched(int oldR, int oldC, int newR, int newC);

    /**
     *
     * @return an array filled with coordinates a Piece touches when making a move
     */
    public Point[] getSquaresTouched(){
        return this.squaresTouched;
    }

    /**
     * Refresh and set everything in the array to null
     * Purpose: to not potentially interfere in the next move
     */
    public void refreshSquaresTouched(){
        for(int i=0; i<this.squaresTouched.length; i++){
            this.squaresTouched[i] = null;
        }
    }

    /**
     * Fills up an array of points that a piece can move to based on its current position
     * Does not take into account blocking or whether this move puts king in check
     *
     * @param row
     * @param col
     */
    public abstract void setValidMovesList(int row, int col);

    /**
     *
     * @return the validMovesList array
     */
    public abstract Point[] getValidMovesList();

//    /**
//     * Refresh and set everything in the array to null
//     * Purpose: to not potentially interfere in the next move
//     */
//    public void refreshValidMovesList(){
//        for(int i=0; i<validMovesList.length; i++){
//            validMovesList[i] = null;
//        }
//    }

}