package chess;

import java.util.ArrayList;

import static chess.Chess.*;

/**
 * Board class
 * Chessboard is a 2D array
 * 1) Places piece objects in their appropriate initial position
 * 2) Prints chess board
 * 3) Parses/Translates move input
 * 4) Check whether move is valid or not
 * 5) Executes move
 * 6) Accounts for check/checkmate/stalemate/draws/resigns/castling
 * 6) Ends Game
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Board {

    /**
     * This is the board object
     */
    public static Piece[][] board;

    /**
     * A copy of the current board with all the pieces
     */
    public static Piece[][] boardReplica = new Piece[8][8];

    /**
     * The piece the player chooses to move
     */
    public static Piece chosenPiece;

    /**
     * If there is a piece at the destination of chosenPiece's move
     */
    public static Piece otherPiece;

    /**
     * Whether or nor the new spot a player wants to move its piece is empty or not (used for pawns)
     */
    public static boolean isNewSpotEmpty = false;

    /**
     * Whether a move is valid and appropriate for the specific chess piece (does not account for blocking)
     */
    public static boolean canMove = true;

    /**
     * If anything is blocking a piece from making a move (does not pertain to Knights)
     */
    public static boolean inLineOfSight = true;

    /**
     * An array of coordinates on the chess Board that a piece crosses when making a move
     */
    public static Point[] squaresTouched;

    /**
     * Whether a player makes a draw offer
     */
    public static boolean drawOffered = false;

    /**
     * Stores the piece that the player wants the pawn to promote into default is the Queen piece
     */
    public static String pawnPromotesTo = "";

    /**
     * The coordinates (old and new) of a chess board chosen by the player for a general chess piece move
     */
    public static int oldC, oldR, newC, newR;

    /**
     * If the move input does not fall into any category of parseMoveInput, it's illegal
     */
    public static boolean moveIllegal = false;

    /**
     * Constructor that creates a board instance and initializes its Pieces
     */
    public Board() {
        board = new Piece[8][8];
        initializeBoard();
    }

    /**
     * 1) Fills the board 2D array with nulls
     * 2) Places the chess Pieces
     */
    public void initializeBoard() {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = null;
            }
        }

        // White Pieces
        board[6][0] = new Pawn(true, "p");
        board[6][1] = new Pawn(true, "p");
        board[6][2] = new Pawn(true, "p");
        board[6][3] = new Pawn(true, "p");
        board[6][4] = new Pawn(true, "p");
        board[6][5] = new Pawn(true, "p");
        board[6][6] = new Pawn(true, "p");
        board[6][7] = new Pawn(true, "p");

        board[7][0] = new Rook(true, "R");
        board[7][1] = new Knight(true, "N");
        board[7][2] = new Bishop(true, "B");
        board[7][3] = new Queen(true, "Q");
        board[7][4] = new King(true, "K");
        board[7][5] = new Bishop(true, "B");
        board[7][6] = new Knight(true, "N");
        board[7][7] = new Rook(true, "R");

        // Black Pieces
        board[1][0] = new Pawn(false, "p");
        board[1][1] = new Pawn(false, "p");
        board[1][2] = new Pawn(false, "p");
        board[1][3] = new Pawn(false, "p");
        board[1][4] = new Pawn(false, "p");
        board[1][5] = new Pawn(false, "p");
        board[1][6] = new Pawn(false, "p");
        board[1][7] = new Pawn(false, "p");

        board[0][0] = new Rook(false, "R");
        board[0][1] = new Knight(false, "N");
        board[0][2] = new Bishop(false, "B");
        board[0][3] = new Queen(false, "Q");
        board[0][4] = new King(false, "K");
        board[0][5] = new Bishop(false, "B");
        board[0][6] = new Knight(false, "N");
        board[0][7] = new Rook(false, "R");
    }

    /**
     * Prints out the syntax/design of the board, as well as the Piece names
     */
    public void printBoard() {

        /**
         *  The 2D String that will be used to print out the board
         */
        String[][] result = new String[8][8];

        // 1. Fill result 2D array with appropriate colors
        // white == "  ", black == "##"
        /**
         * Whether the tile should be colored black or white
         */
        boolean whiteTile = true;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (whiteTile) {
                    result[i][j] = "  ";
                    whiteTile = false;
                } else {
                    result[i][j] = "##";
                    whiteTile = true;
                }
            }
            whiteTile = !(whiteTile);
        }

        // 2. Fill result 2D array with appropriately  placed chess pieces
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (board[x][y] != null)
                    result[x][y] = board[x][y].getPieceName();
            }
        }

        // 3. Print out the result board
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                System.out.print(result[x][y] + " ");
            }
            //System.out.print(x);
            System.out.print(8 - x);
            System.out.println();
        }
        //System.out.println(" 0  1  2  3  4  5  6  7");
        System.out.println(" a  b  c  d  e  f  g  h");

    }

    /**
     * Handles checking if the move is valid and executing it
     *
     * @param moveInput
     * @param isWhitesTurn
     * @return true == move executed properly, false == illegal move
     */
    public static boolean movePiece(String moveInput, boolean isWhitesTurn) {

        //O.O Illegal flag, no 4 char words that are not "draw"
        if(moveInput.length() <= 4 && !(moveInput.equals("draw")))
            return false;
        if(drawOffered == false && moveInput.equals("draw"))
            return false;

        // 1. Parse Input to get the 2 coordinates
        parseMoveInput(moveInput, isWhitesTurn);

        //1.5 Check if moveIllegal (out of bounds move, for example) is true
        if(moveIllegal){
            moveIllegal = false;
            return false;
        }

        // 2. See what chess piece lies in (oldX, oldY)
        if(board[oldR][oldC] == null) return false;
        chosenPiece = board[oldR][oldC];

        // 2.5 If other piece is the same color, or is the king, flag as illegal
        if(board[newR][newC] != null){
            otherPiece = board[newR][newC];
            if(chosenPiece.isWhite() == otherPiece.isWhite())
                return false;
            if(otherPiece.getPieceName().charAt(1) == 'K')
                return false;
        }

        // 3. Call checkMove with chosenPiece and the new X/Y to see if moveInput is valid
        if (board[newR][newC] == null) isNewSpotEmpty = true;
        else isNewSpotEmpty = false;
        if(!isNewSpotEmpty && chosenPiece.isWhite == board[newR][newC].isWhite){ return false; }
        canMove = chosenPiece.checkMove(oldR, oldC, newR, newC, isNewSpotEmpty);

        // 4. Formulate and retrieve the array of squares this Piece would touch
        chosenPiece.setSquaresTouched(oldR, oldC, newR, newC);
        squaresTouched = chosenPiece.getSquaresTouched();

        // 5. Check if anything is blocking it, or is in its line of Sight (does not pertain to knights)
        if (!chosenPiece.name.contains("N") || !chosenPiece.name.contains("K") || !chosenPiece.name.contains("p"))
            inLineOfSight = lineOfSight(squaresTouched);
        chosenPiece.refreshSquaresTouched();

        // 5.9. Find the locations of both kings

        /**
         * Coordinates of your king
         */
        Point your_king_point = findKing(isWhitesTurn);

        /**
         * Your king Object
         */
        King your_king = (King)board[your_king_point.getRow()][your_king_point.getCol()];

        /**
         * Coordinates of his king
         */
        Point his_king_point = findKing(!isWhitesTurn);

        /**
         * His king Object
         */
        King his_king = (King)board[his_king_point.getRow()][his_king_point.getCol()];

        // 6. If move is valid and nothing is blocking, execute the move
        if (canMove == true && (inLineOfSight == true || chosenPiece.name.contains("N") || chosenPiece.name.contains("K") || chosenPiece.name.contains("p"))) {
            copyBoard();
            execute(isWhitesTurn);
            your_king_point = findKing(isWhitesTurn);
            your_king = (King)board[your_king_point.getRow()][your_king_point.getCol()];
            if(checkCheck(your_king_point.getRow(), your_king_point.getCol(), !isWhitesTurn)){
                rollBackBoard();
                return false;
            }
            your_king.inCheck = false;
            if(checkCheck(his_king_point.getRow(), his_king_point.getCol(), isWhitesTurn)){
                his_king.inCheck = true;
                System.out.println("Check\n");
            }
            getGameStatus(isWhitesTurn);
            return true;
        }else if(chosenPiece.name.charAt(1) == 'K' && !checkCheck(your_king_point.getRow(), your_king_point.getCol(), !isWhitesTurn) && castling()){
                your_king.inCheck = false;
                if(checkCheck(his_king_point.getRow(), his_king_point.getCol(), isWhitesTurn)){
                    his_king.inCheck = true;
                    System.out.println("Check\n");
                }
            getGameStatus(isWhitesTurn);
                return true;
        }
        return false;
    }

    /**
     * Parses the input whenever a player makes a move.
     * Also accounts for unique cases:
     * <p>
     * Unique Cases:
     * 1) Offering Draw: "e2 e4 draw?"
     * 2) Accepting Draw: "draw"
     * 3) Resign: "resign"
     *
     * @param moveInput, what the user inputs as its move
     * @param isWhitesTurn, whether if is white's or black's turn
     */
    public static void parseMoveInput(String moveInput, boolean isWhitesTurn) {

        /*
            COLUMN (y value)        ROW (x value)
            a = 0                   8 = 0
            b = 1                   7 = 1
            c = 2                   6 = 2
            d = 3                   5 = 3
            e = 4                   4 = 4
            f = 5                   3 = 5
            g = 6                   2 = 6
            h = 7                   1 = 7
            Ex) e2 e4 = [2][e] [4][e] = [row][column] = [6][4] [4][4]
         */

        //Unique Cases
        moveInput = moveInput.trim();
        if(moveInput.equals("resign")){
            if (isWhitesTurn == true) System.out.println("Black wins");
            else System.out.println("White wins");
            endGame();
        }
        else if (moveInput.length() > 5){
            if(moveInput.substring(6).equals("draw?")){
                drawOffered = true;
            }
            else if(moveInput.substring(6).length() == 1){
                pawnPromotesTo = moveInput.substring(6);
            }
        }
        else if (moveInput.equals("draw") && drawOffered == true){
            endGame();
        }

        //General input: "FileRank FileRank"
        oldC = translate(moveInput.charAt(0) + "");
        oldR = translate(moveInput.charAt(1) + "");
        newC = translate(moveInput.charAt(3) + "");
        newR = translate(moveInput.charAt(4) + "");

        if(oldR > 7 || newR > 7) moveIllegal = true;
    }

    /**
     * Used for parseMoveInput(), to translate the letter/number input into the column/row value, respectively
     *
     * @param c, a character
     * @return the corresponding int value
     */
    public static int translate(String c) {

        if (c.equals("a") || c.equals("8"))
            return 0;
        else if (c.equals("b") || c.equals("7"))
            return 1;
        else if (c.equals("c") || c.equals("6"))
            return 2;
        else if (c.equals("d") || c.equals("5"))
            return 3;
        else if (c.equals("e") || c.equals("4"))
            return 4;
        else if (c.equals("f") || c.equals("3"))
            return 5;
        else if (c.equals("g") || c.equals("2"))
            return 6;
        else
            return 7;

    }

    /**
     * Whether or not the origin and destination of a move for a Piece is not blocked by any other piece (Knights
     * excluded)
     *
     * @param squaresTouched, an array of points on the board where a piece would cross if it were to execute move
     * @return true = clear to move, false = another piece is blocking
     */
    public static boolean lineOfSight(Point[] squaresTouched) {
        //Check squares between origin and destination
        if(squaresTouched[0] == null)
            return true;
        for (Point point : squaresTouched) {
            if (point != null) {
                if (board[point.getRow()][point.getCol()] != null)
                    return false;
            }
        }
        return true;
    }

    /**
     * If 1) Move is valid based on the Piece, and
     * 2) There is no piece blocking it (does not matter for Knights)
     * execute the move, meaning actually moving the piece on the board
     *
     * @param isWhitesTurn
     */
    public static void execute(boolean isWhitesTurn) {
        // 7. Check if this is a pawn promotion situation
        if (chosenPiece.name.charAt(1) == 'p' && (newR == 7 || newR == 0)) {
            if (pawnPromotesTo.equals("B")) {
                board[newR][newC] = new Bishop(isWhitesTurn, "B");
            } else if (pawnPromotesTo.equals("N")) {
                board[newR][newC] = new Knight(isWhitesTurn, "N");
            } else if (pawnPromotesTo.equals("R")) {
                board[newR][newC] = new Rook(isWhitesTurn, "R");
            } else {//Default is to promote to Queen
                board[newR][newC] = new Queen(isWhitesTurn, "Q");
            }
            pawnPromotesTo = "";
        }else{
            switch(board[oldR][oldC].name.charAt(1)){
                case 'p':
                    board[newR][newC] = new Pawn((Pawn) board[oldR][oldC]);
                    break;
                case 'K':
                    board[newR][newC] = new King((King) board[oldR][oldC]);
                    break;
                case 'N':
                    board[newR][newC] = new Knight((Knight) board[oldR][oldC]);
                    break;
                case 'B':
                    board[newR][newC] = new Bishop((Bishop) board[oldR][oldC]);
                    break;
                case 'R':
                    board[newR][newC] = new Rook((Rook) board[oldR][oldC]);
                    break;
                case 'Q':
                    board[newR][newC] = new Queen((Queen) board[oldR][oldC]);
                    break;
            }
        }
        board[oldR][oldC] = null;
    }

    /**
     * End Game
     * 1) When either Black or White win
     * 2) A player offers to draw and the other player accepts
     * 3) When a player resigns
     */
    public static void endGame() {
        System.exit(0);
    }

    /**
     * Castling:
     * 1) Checks if all conditions are correct
     * 2) Executes the move of the King and Rook
     *
     * @return whether castling is applicable or not
     */
    public static boolean castling() {
        //5.2 Check the validity of Castling
        if (chosenPiece.isFirstMove) {//Check if the king has every move
            if (newR == 0 && !isWhitesTurn) {//black side
                if (newC == 2) {//castling queen side
                    if (board[newR][newC] == null && board[newR][newC - 1] == null && board[newR][newC + 1] == null) {//checks if spots are empty
                        if (board[newR][newC - 2].isFirstMove && !checkCheck(newR, newC, true) && !checkCheck(newR , newC + 1, true)) {//check if the rook has ever move and whether the king enters check or not
                            board[newR][newC] = new King((King) board[oldR][oldC]);
                            board[newR][newC + 1] = new Rook((Rook) board[newR][newC - 2]);
                            board[oldR][oldC] = null;
                            board[newR][newC - 2] = null;
                            board[newR][newC].isFirstMove = false;
                            board[newR][newC + 1].isFirstMove = false;
                            return true;
                        }
                    }/*
                 Result:  Black Castle Queen Side
                 New Position:
                 board[newR][newC] = Black King
                 board[newR][newC + 1] = Black Queen-side Rook
                 */
                } else if (newC == 6) {//castling king side
                    if (board[newR][newC] == null && board[newR][newC - 1] == null) {//checks if spots are empty
                        if (board[newR][newC + 1].isFirstMove && !checkCheck(newR, newC, true) && !checkCheck(newR , newC - 1, true)) {//check if the rook has ever move and whether the king enters check or not
                            board[newR][newC] = new King((King) board[oldR][oldC]);
                            board[newR][newC - 1] = new Rook((Rook) board[newR][newC + 1]);
                            board[oldR][oldC] = null;
                            board[newR][newC + 1] = null;
                            board[newR][newC].isFirstMove = false;
                            board[newR][newC -1].isFirstMove = false;
                            return true;
                        }
                    }
                }/*
             Result:  Black Castle King Side
             New Position:
             board[newR][newC] = Black King
             board[newR][newC - 1] = Black King-side Rook
             */
            } else if (newR == 7 && isWhitesTurn) {//white side
                if (newC == 2) {//castling queen side
                    if (board[newR][newC] == null && board[newR][newC - 1] == null && board[newR][newC + 1] == null) {//checks if spots are empty
                        if (board[newR][newC - 2].isFirstMove && !checkCheck(newR, newC, false) && !checkCheck(newR, newC + 1, false)) {//check if the rook has ever move and whether the king enters check or not
                            board[newR][newC] = new King((King) board[oldR][oldC]);
                            board[newR][newC + 1] = new Rook((Rook) board[newR][newC - 2]);
                            board[oldR][oldC] = null;
                            board[newR][newC - 2] = null;
                            board[newR][newC].isFirstMove = false;
                            board[newR][newC + 1].isFirstMove = false;
                            return true;
                        }
                    }/*
                 Result:  White Castle Queen Side
                 New Position:
                 board[newR][newC] = White King
                 board[newR][newC + 1] = White Queen-side Rook
                 */
                } else if (newC == 6) {//castling king side
                    if (board[newR][newC] == null && board[newR][newC - 1] == null) {//checks if
                        // spots are empty
                        if (board[newR][newC + 1].isFirstMove && !checkCheck(newR, newC, false) && !checkCheck(newR, newC - 1, false)) {//check if the rook has ever move and whether the king enters check or not
                            board[newR][newC] = new King((King) board[oldR][oldC]);
                            board[newR][newC - 1] = new Rook((Rook) board[newR][newC + 1]);
                            board[oldR][oldC] = null;
                            board[newR][newC + 1] = null;
                            board[newR][newC].isFirstMove = false;
                            board[newR][newC - 1].isFirstMove = false;
                            return true;
                        }
                    }/*
                 Result:  White Castle King Side
                 New Position:
                 board[newR][newC] = White King
                 board[newR][newC - 1] = White King-side Rook
                 */
                }
            }
        }
        return false;
    }
    /**
     * Checks if a square is being attacked by a piece from opponent side
     * or
     * Checks if a particular square is under check
     * R and C is the coordinates of the square that we want to check if its under check
     * if isWhiteAttacking = True then we are checking if white pieces are attacking a specific square
     *
     * @param R = row
     * @param C = col
     * @param isWhiteAttacking = whether white is attacking or not
     * @return = whether king is in check or not
     */
    public static boolean checkCheck(int R, int C, boolean isWhiteAttacking) {

        /**
         * If new spot is empty when attacking
         */
        boolean isNewSpotEmpty = board[R][C] == null ? true : false;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece piece = board[i][j];
                if (piece == null) continue;
                if (isWhiteAttacking == piece.isWhite) {
                    if (piece.name.charAt(1) == 'p') {//if pawn we do something special because it attacks
                        // diagonally but moves differently
                        if (checkPawnAttack(i, j, R, C, piece.isWhite)) {
                            return true;
                        }
                    } else if(piece.checkMove(i, j, R, C, isNewSpotEmpty)){
                            if (!piece.name.contains("N") || !piece.name.contains("K") || !piece.name.contains("p")) {
                                piece.setSquaresTouched(i, j, R, C);
                                if (lineOfSight(piece.getSquaresTouched())) {
                                    piece.refreshSquaresTouched();
                                    return true;
                                }
                                piece.refreshSquaresTouched();
                            }else{
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Use to check if a pawn is checking a  particular square
     *
     * @param oldR
     * @param oldC
     * @param newC
     * @param newR
     * @param isWhitePiece
     * @return - true == this pawn is attack there | false == cannot attack there
     */
    public static boolean checkPawnAttack(int oldR, int oldC, int newR, int newC, boolean isWhitePiece) {
        if (isWhitePiece) {
            if (Math.abs(newC - oldC) == 1 && ((newR - oldR) == -1)) {//go diagonal to capture //does not account for whether moving to a square that is occupied by your own piece
                return true;//Additional Checks needed to make sure that this diagonal piece is an enemy piece and not the enemy king
            }
        } else {
            if (Math.abs(newC - oldC) == 1 && ((newR - oldR) == 1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Given the color of the king finds the position of this king
     *
     * @param isWhiteKing
     * @return - a point object with the array indices of the king
     */
    public static Point findKing(boolean isWhiteKing){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                /**
                 * A piece in a board
                 */
                Piece piece = board[i][j];
                if (piece == null) continue;
                if (isWhiteKing) {
                    if (piece.name.equals("wK")) {
                        return new Point(i, j);
                    }
                } else {
                    if (piece.name.equals("bK")) {
                        return new Point(i, j);
                    }
                }
            }
        }
        return null;
    }

    /**
     * If whitesTurn check if you have checkmate black and vice versa
     * Depending on who's turn it is we are looking for the opposite king so isWhiteAttacking = true -> we are
     * checkmating black king and vice versa
     *
     * @param isWhiteAttacking
     * @return - if there's a check mate
     */
    public static boolean checkMate(boolean isWhiteAttacking) {
        /**
         * King's coordinate points on the board
         */
        Point king_point = findKing(!isWhiteAttacking);

        /**
         * Row and Col of King
         */
        int R = king_point.getRow(), C = king_point.getCol();

        /**
         * King object
         */
        King king = (King) board[R][C];
        /*
        1) Observe the squares adjacent to the king positions
        If any of those squares are possible moves then the king is not in check
         */
        copyBoard();//Copy the original board we will bring back the board if we the king is not in checkMate
        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                if(i == 0 && j == 0) continue;
                int tempR = R + i;
                int tempC = C + j;
                if((tempR < 8 && tempR >= 0) && (tempC < 8 && tempR >= 0)){
                    if(board[tempR][tempC] == null){//the square has to be unoccupied
                        oldR = R;
                        oldC = C;
                        newR = tempR;
                        newC = tempC;
                        execute(!isWhiteAttacking);
                        if(!checkCheck(tempR, tempC, isWhiteAttacking)){
                            rollBackBoard();
                            return false;//The king can move to one of these adjacent squares
                        }
                    rollBackBoard();

                    }
                }
            }
        }
        /* Finding the pieces that are attacking the king */
        /**
         * Pieces that are attacking the king
         */
        ArrayList<Point> attackingSquares = new ArrayList<Point>();
        if(!king.inCheck) {
            return false;
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                /**
                 * A piece from the board
                 */
                Piece piece = board[i][j];
                if (piece == null) continue;
                //The reason I didn't use checkCheck here is because checkCheck does not return the piece that is attacking a specific square
                if (king.isWhite != piece.isWhite) {//look for the opposite color
                    if (piece.name.charAt(1) == 'p') {
                        if (checkPawnAttack(i, j, R, C, piece.isWhite)) {
                            attackingSquares.add(new Point(i, j));
                        }
                    } else if (piece.checkMove(i, j, R, C, false)) {
                        if (!piece.name.contains("N") || !piece.name.contains("K") || !piece.name.contains("p")) {
                            piece.setSquaresTouched(i, j, R, C);
                            if (lineOfSight(piece.getSquaresTouched())) {
                                attackingSquares.add(new Point(i, j));
                            }
                        }
                        piece.refreshSquaresTouched();
                    }
                }
            }
        }

        //if the number of successful attacking squares on the king is more than 2 than its impossible to block/capture both without moving the king
        if(attackingSquares.size() > 1){
            return true;
        }
        if(attackingSquares.size() == 0 ){
            return false;
        }
        //Only one piece that is attacking the king at this point
        /* The idea here is to run simulation of the chess board through brute forcing possible legal moves that the player can take
        1. Capture the single piece with his piece at most 16 possible moves to calculate
        2. Simulate blocking the line of sight of this piece does not apply for knight | does apply for bishop, rook, and queen at most 6 * 15 (subtract king) possible moves to calculate
         */

        /**
         * Attacker's row
         */
        int attacking_R = attackingSquares.get(0).getRow();

        /**
         * Attacker's col
         */
        int attacking_C = attackingSquares.get(0).getCol();

        /**
         * The piece that is attacking the king
         */
        Piece attacking_piece = board[attacking_R][attacking_C];


        /*
        2)Can he capture the piece that I am attacking him with
        Given that one of our pieces is attacking the other king (every piece except for the king) then is it possible for him to capture the piece that is checking his king
         */
        for(int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                /**
                 * A piece from the board
                 */
                Piece piece = board[i][j];
                if (piece == null) continue;
                if (king.isWhite == piece.isWhite) {
                    if (piece.name.charAt(1) == 'p') {//if pawn we do something special because it attacks
                        // diagonally but moves differently
                        if (checkPawnAttack(i, j, attacking_R, attacking_C, piece.isWhite)) {
                            oldR = i;
                            oldC = j;
                            newR = attacking_R;
                            newC = attacking_C;
                            execute(king.isWhite);
                            if (checkCheck(king_point.getRow(), king_point.getCol(), !king.isWhite)) {//This is the
                                // poor man's recursion (reminds me of Backtracking) //if his king is still in check
                                // then its still checkmate
                                rollBackBoard();
                                piece.refreshSquaresTouched();
                            } else {
                                rollBackBoard();
                                piece.refreshSquaresTouched();
                                return false;
                            }
                        }
                    }else if (piece.checkMove(i, j, attacking_R, attacking_C, isNewSpotEmpty)) {
                            if (!piece.name.contains("N") || !piece.name.contains("K") || !piece.name.contains("p")) {
                                piece.setSquaresTouched(i, j, R, C);
                                if (lineOfSight(piece.getSquaresTouched())) {
                                    oldR = i;
                                    oldC = j;
                                    newR = attacking_R;
                                    newC = attacking_C;
                                    execute(king.isWhite);
                                    king_point = findKing(king.isWhite);
                                    if (checkCheck(king_point.getRow(), king_point.getCol(), !king.isWhite)) {//This
                                        // is the poor man's recursion (reminds me of Backtracking) //if his king is
                                        // still in check then its still checkmate
                                        rollBackBoard();
                                        piece.refreshSquaresTouched();
                                    } else {
                                        rollBackBoard();
                                        piece.refreshSquaresTouched();
                                        return false;
                                    }
                                }
                            }
                        }
                }
            }
        }
        /*
        3)Can he block my check with one of his piece
        Attack Side: if the piece is pawn/knight than its over else if bishop/rook/queen theres still a chance
        Defense Side: try every piece there is except for the king because if the king was able to move we wouldn't be at this step (remember step 1?) and if the piece was a pawn it would be checked by step 2
         */
        if(attacking_piece.name.charAt(1) == 'N'){
            return true;
        }else{// bishop/rook/queen
            if(attacking_piece.name.charAt(1) != 'B' || attacking_piece.name.charAt(1) != 'R' || attacking_piece.name.charAt(1) != 'Q'){
                return true;
            }
            attacking_piece.setSquaresTouched(attacking_R, attacking_C, R, C);
            for (Point point : attacking_piece.getSquaresTouched()){
                for(int i = 0; i < 8; i++) {
                    for (int j = 0; j < 8; j++) {

                        /**
                         * The blocking piece
                         */
                        Piece blocking_piece  = board[i][j];
                        if (blocking_piece == null || blocking_piece.name.charAt(1) == 'K'){ continue; }//skip nulls or kings squares
                        if (king.isWhite == blocking_piece.isWhite) {//match the color to the enemy king
                            canMove = blocking_piece.checkMove(i, j, point.getRow(), point.getCol(), true);//check if the piece can move from its original spot to the new blocking spot
                            blocking_piece.setSquaresTouched(i, j, point.getRow(), point.getCol());//lets see if anything is blockings its path
                            squaresTouched = blocking_piece.getSquaresTouched();
                            if (!blocking_piece.name.contains("N") || !blocking_piece.name.contains("K") || !blocking_piece.name.contains("p")) {
                                inLineOfSight = lineOfSight(squaresTouched);
                            }
                            blocking_piece.refreshSquaresTouched();
//                            attacking_piece.refreshSquaresTouched();
                            if (canMove == true && (inLineOfSight == true || chosenPiece.name.contains("N")) || chosenPiece.name.contains("K") || chosenPiece.name.contains("p")) {
                                oldR = i;
                                oldC = j;
                                newR = attacking_R;
                                newC = attacking_C;
                                execute(!isWhitesTurn);
                                if (checkCheck(R, C, isWhiteAttacking)) {//This is the poor man's recursion (reminds me of Backtracking)
                                    rollBackBoard();
                                } else {
                                    rollBackBoard();
                                    attacking_piece.refreshSquaresTouched();
                                    return false;//Not checkmate because opponent was able to capture one of your piece
                                }
                            }
                        }
                    }
                }
            }
        }
        attacking_piece.refreshSquaresTouched();
        return true;
    }

    /**
     * Makes a deep copy of the board
     */
    public static void copyBoard(){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(board[i][j] == null) {
                    boardReplica[i][j] = null;
                    continue;
                }
                switch(board[i][j].name.charAt(1)){
                    case 'p':
                        boardReplica[i][j] = new Pawn((Pawn) board[i][j]);
                        break;
                    case 'K':
                        boardReplica[i][j] = new King((King) board[i][j]);
                        break;
                    case 'N':
                        boardReplica[i][j] = new Knight((Knight) board[i][j]);
                        break;
                    case 'B':
                        boardReplica[i][j] = new Bishop((Bishop) board[i][j]);
                        break;
                    case 'R':
                        boardReplica[i][j] = new Rook((Rook) board[i][j]);
                        break;
                    case 'Q':
                        boardReplica[i][j] = new Queen((Queen) board[i][j]);
                        break;
                }
            }
        }
    }

    /**
     * Restores Original Board
     */
    public static void rollBackBoard(){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                if(boardReplica[i][j] == null){
                    board[i][j] = null;
                    continue;
                }
                switch(boardReplica[i][j].name.charAt(1)){
                    case 'p':
                        board[i][j] = new Pawn((Pawn) boardReplica[i][j]);
                        break;
                    case 'K':
                        board[i][j] = new King((King) boardReplica[i][j]);
                        break;
                    case 'N':
                        board[i][j] = new Knight((Knight) boardReplica[i][j]);
                        break;
                    case 'B':
                        board[i][j] = new Bishop((Bishop) boardReplica[i][j]);
                        break;
                    case 'R':
                        board[i][j] = new Rook((Rook) boardReplica[i][j]);
                        break;
                    case 'Q':
                        board[i][j] = new Queen((Queen) boardReplica[i][j]);
                        break;
                }
            }
        }
    }

    /**
     * Checks if the game is in a stalemate
     * Meaning, if all of your pieces have no valid moves
     */
    public static boolean stalemate(boolean isWhitesTurn) {
        copyBoard();
        /**
         * Array that is a list of moves a piece can move to
         */
        Point[] validMovesList = null;
        /**
         * The specific white/black piece we are focusing on
         */
        Piece focusedPiece = null;
        /**
         * The squares between origin and destination in a move
         */
        Point[] squaresTouchedTest;
        /**
         * Whether anything is blocking the move or not
         */
        boolean inLineOfSightTest;
        /**
         * Location of your king
         */
        int kingRow = 0, kingCol = 0;

        // 1. Create an array for all black and all white pieces
        /**
         * An array of all of the white piece coordinates
         */
        Point[] whitePieces = new Point[16];
        /**
         * An array of all of the black piece coordinates
         */
        Point[] blackPieces = new Point[16];
        /**
         * Counts how many pieces of each color there are
         */
        int blackCounter = 0, whiteCounter = 0;

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board[i][j] != null) {
                    if (board[i][j].isWhite) {
                        whitePieces[whiteCounter] = new Point(i, j);
                        whiteCounter++;
                    } else {
                        blackPieces[blackCounter] = new Point(i, j);
                        blackCounter++;
                    }
                }
            }
        }
        if(whiteCounter == 1 && blackCounter == 1){//only white king and black king left so automatic draw
            return true;
        }
        // 2. LOOP through every piece on White's team (if its White's turn)
        if (!isWhitesTurn) {
            for (int i = 0; i < whitePieces.length; i++) {
                if (whitePieces[i] != null) {
                    // 1. Retrieve all moves one piece can go to - Now every piece has an array of moves
                    focusedPiece = board[whitePieces[i].getRow()][whitePieces[i].getCol()];
                    focusedPiece.setValidMovesList(whitePieces[i].getRow(), whitePieces[i].getCol());
                    validMovesList = focusedPiece.getValidMovesList();

                    // 2. LOOP through every valid move for one piece and filter it
                    for (int j = 0; j < validMovesList.length; j++) {
                        if (validMovesList[j] != null) {

                            // 1. Check LineOfSight
                            focusedPiece.setSquaresTouched(whitePieces[i].getRow(), whitePieces[i].getCol(), validMovesList[j].getRow(), validMovesList[j].getCol());
                            squaresTouchedTest = focusedPiece.getSquaresTouched();
                            inLineOfSightTest = lineOfSight(squaresTouchedTest);
                            focusedPiece.refreshSquaresTouched();
                            if (inLineOfSightTest != true) {
                                validMovesList[j] = null;
                                continue;
                            }

                            // 2. Check if any destination square has a piece of the same color
                            if (validMovesList[j] != null && board[validMovesList[j].getRow()][validMovesList[j].getCol()] != null) {
                                if (board[validMovesList[j].getRow()][validMovesList[j].getCol()].isWhite() == true) {
                                    validMovesList[j] = null;
                                    continue;
                                }
                            }
                            /* Test Purposes Display Valid */
                            //if (validMovesList[j] != null)
                            //System.out.println("Piece: " + whitePieces[i].toString() + " Move: " + validMovesList[j].toString());

                            // 3. In a new board IF the 'valid' move from the white piece has been executed, would the king be in check?
                            // create a copy board
                            // in copy board, execute the move
                            // see if king is in check
                            oldR = whitePieces[i].getRow();
                            oldC = whitePieces[i].getCol();
                            newR = validMovesList[j].getRow();
                            newC = validMovesList[j].getCol();
                            execute(isWhitesTurn);

                            // Find White King's location
                            for (int k = 0; k < 8; k++) {
                                for (int g = 0; g < 8; g++) {
                                    if (board[k][g] == null) continue;
                                    if (board[k][g].getPieceName().equals("wK")) {
                                        kingRow = k;
                                        kingCol = g;
                                        break;
                                    }
                                }
                            }
                            if (checkCheck(kingRow, kingCol, isWhitesTurn)) {//This is the poor man's recursion (reminds
                                validMovesList[j] = null;
                            }
                            rollBackBoard();
                        }
                    /*
                    1) Need to make sure, for the King, if all valid castling moves are in the king array too
                    */

//                    // 3. Check if there is at least one valid move left in the array, if there is none, its a stalemate
                    }
                }

                int count = 0;
                for (int a = 0; a < validMovesList.length; a++) {
                    if (validMovesList[a] != null)
                        count++;
                }
                if (count >= 1) {
                    return false;
                }
            }
        }
        // 2. LOOP through every piece on Black's team (if its Blacks's turn)
        else {
            for (int i = 0; i < blackPieces.length; i++) {
                if (blackPieces[i] != null) {
                    // 1. Retrieve all moves one piece can go to - Now every piece has an array of moves
                    focusedPiece = board[blackPieces[i].getRow()][blackPieces[i].getCol()];
                    focusedPiece.setValidMovesList(blackPieces[i].getRow(), blackPieces[i].getCol());
                    validMovesList = focusedPiece.getValidMovesList();

                    // 2. LOOP through every valid move for one piece and filter it
                    for (int j = 0; j < validMovesList.length; j++) {
                        if (validMovesList[j] != null) {

                            // 1. Check LineOfSight
                            focusedPiece.setSquaresTouched(blackPieces[i].getRow(), blackPieces[i].getCol(), validMovesList[j].getRow(), validMovesList[j].getCol());
                            squaresTouchedTest = focusedPiece.getSquaresTouched();
                            inLineOfSightTest = lineOfSight(squaresTouchedTest);
                            if (inLineOfSightTest != true) {
                                validMovesList[j] = null;
                                continue;
                            }

                            // 2. Check if any destination square has a piece of the same color
                            if (validMovesList[j] != null && board[validMovesList[j].getRow()][validMovesList[j].getCol()] != null) {
                                if (board[validMovesList[j].getRow()][validMovesList[j].getCol()].isWhite() == false) {
                                    validMovesList[j] = null;
                                    continue;
                                }
                            }
                            /* Test Purposes Display Valid */
                            //if (validMovesList[j] != null)
                            //System.out.println("Piece: " + blackPieces[i].toString() + " Move: " + validMovesList[j].toString());

                            // 3. In a new board IF the 'valid' move from the white piece has been executed, would the king be in check?
                            // create a copy board
                            // in copy board, execute the move
                            // see if king is in check
                            oldR = blackPieces[i].getRow();
                            oldC = blackPieces[i].getCol();
                            newR = validMovesList[j].getRow();
                            newC = validMovesList[j].getCol();
                            execute(isWhitesTurn);

                            // Find White King's location
                            for (int k = 0; k < 8; k++) {
                                for (int g = 0; g < 8; g++) {
                                    if (board[k][g] == null) continue;
                                    if (board[k][g].getPieceName().equals("bK")) {
                                        kingRow = k;
                                        kingCol = g;
                                        break;
                                    }
                                }
                            }
                            if (checkCheck(kingRow, kingCol, isWhitesTurn)) {//This is the poor man's recursion (reminds
                                validMovesList[j] = null;
                            }
                            rollBackBoard();
                            focusedPiece.refreshSquaresTouched();
                        }

                    /*
                    1) Need to make sure, for the King, if all valid castling moves are in the king array too
                    */

//                    // 3. Check if there is at least one valid move left in the array, if there is none, its a stalemate
                    }
                }
                    int count = 0;
                    for (int a = 0; a < validMovesList.length; a++) {
                        if (validMovesList[a] != null)
                            count++;
                    }
                    if (count >= 1) {
                        return false;
                    }
                }
            }
            return true;
        }


        /**
         * Determines if the game ended, or there is a tie
         */
        public static void getGameStatus(boolean turn){
            /**
             * Where opponent's king is
             */
            Point his_king_point = findKing(!turn);
            if (checkCheck(his_king_point.getRow(), his_king_point.getCol(), turn)) {
                if (checkMate(turn)) {
                    gameBoard.printBoard();
                    System.out.println();
                    System.out.println("Checkmate");
                    {
                        if (turn) {
                            System.out.println("White wins");
                        } else {
                            System.out.println("Black wins");
                        }
                    }
                    endGame();
                }
            } else {
                if (stalemate(turn)) {
                    gameBoard.printBoard();
                    System.out.println();
                    System.out.println("draw");
                    endGame();
                }
            }
        }
    }






